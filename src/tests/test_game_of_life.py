from tomato.rules import game_of_life as rule
import tomato as tt
import numpy as np

resources_path = "resources"
saved_state_matrix_path = f"{resources_path}/game_of_life.npy"


def run_generations(gens=5, size=(20, 20)):
    # {{{
    rng = np.random.default_rng(777)

    state_matrix = rng.choice(2, size)

    board = tt.Board(rule)
    board.start(
        state_matrix,
        show_window=False,
        generations=gens,
    )

    return board.state_matrix


# }}}


def save_generation(gens=5, *args, **kwargs):
    final_state_matrix = run_generations(gens, *args, **kwargs).astype("uint8")
    np.save(saved_state_matrix_path, final_state_matrix)


def load_generation():
    return np.load(saved_state_matrix_path)


def test_game_of_life():
    assert (load_generation() == run_generations(5)).all()
