import numpy as np
import tomato as tt
from tomato.rules import langtons_ant as rule

resources_path = "resources"
saved_state_matrix_path = f"{resources_path}/langtons_ant.npy"


def run_generations(gens=50, size=(20, 20), ant_pos=(10, 10), ant_dir=(1, 0)):
    # {{{
    ant_list = [
        rule.AntTuple((1, 0), (10, 10)),
    ]

    state_matrix = np.zeros(size)

    board = tt.Board(rule)
    board.start(
        state_matrix,
        ant_list,
        show_window=False,
        generations=gens,
    )

    return board.state_matrix


# }}}


def save_generation(gens=50, *args, **kwargs):
    final_state_matrix = run_generations(gens, *args, **kwargs).astype("uint8")
    np.save(saved_state_matrix_path, final_state_matrix)


def load_generation():
    return np.load(saved_state_matrix_path)


def test_langtons_ant():
    assert (load_generation() == run_generations(50)).all()
