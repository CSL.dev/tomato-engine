from tomato.rules import automata1D as rule
import tomato as tt
import numpy as np

resources_path = "resources"
saved_state_matrix_path = f"{resources_path}/automata1D.npy"


def by_decimal_num(num):
    return tuple(bin(num).replace("0b", "").zfill(8))


def run_generations(gens=5, size=(20, 20), rule_num=184):
    # {{{
    rng = np.random.default_rng(777)

    state_matrix = np.zeros(size)
    state_matrix[0, :] = rng.choice(2, size[1])

    board = tt.Board(rule)
    board.start(
        state_matrix,
        cell_args=by_decimal_num(rule_num),
        show_window=False,
        generations=gens,
    )

    return board.state_matrix


# }}}


def save_generation(gens=5, *args, **kwargs):
    final_state_matrix = run_generations(gens, *args, **kwargs).astype("uint8")
    np.save(saved_state_matrix_path, final_state_matrix)


def load_generation():
    return np.load(saved_state_matrix_path)


def test_automata1D():
    assert (load_generation() == run_generations(5)).all()
