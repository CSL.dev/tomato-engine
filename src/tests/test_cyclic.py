from tomato.rules import cyclic as rule
import tomato as tt
import numpy as np

resources_path = "resources"
saved_state_matrix_path = f"{resources_path}/cyclic.npy"


def run_generations(
    gens=5, size=(20, 20), num_states=10, neighborhood="neumann"
):
    # {{{
    rng = np.random.default_rng(777)

    NUM_STATES = 10
    NEIGHBORHOOD = "neumann"
    CELL_ARGS = {"num_states": NUM_STATES, "neighborhood": NEIGHBORHOOD}

    state_matrix = rng.choice(num_states, size)

    board = tt.Board(rule)
    board.start(
        state_matrix,
        cell_args=CELL_ARGS,
        show_window=False,
        generations=gens,
    )

    return board.state_matrix


# }}}


def save_generation(gens=5, *args, **kwargs):
    final_state_matrix = run_generations(gens, *args, **kwargs).astype("uint8")
    np.save(saved_state_matrix_path, final_state_matrix)


def load_generation():
    return np.load(saved_state_matrix_path)


def test_cyclic():
    assert (load_generation() == run_generations(5)).all()
